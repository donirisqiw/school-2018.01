import os

from ....core.document_tokenizer import *


def test_tokenize():
    correct_tokens = ['import', 'smth', '\n', '\n', '0.054545', '\n', '\n', '12445', '\n', '\n', 'def', 'func', '(',
                      'q', ',', 'w', ',', 'e', ',', 'r', ',', 't', ',', 'y', ')', ':', '\n', 'if', 'smth', '==',
                      'another', ':', '\n', 'return', '[', "'", 'text', "'", ',', "'", 'more', "'", ',', '{', "'",
                      'key', "'", ':', '(', 'val1', ',', 'val2', ')', '}', ']', '\n', '#', 'short', 'comment', '\n',
                      '#########', 'wantt', 'more', '#', '\n', '\n', '"""', '\n', 'multiline', 'comment', '\n', '"""',
                      '\n', '\n', "'''", '\n', 'can', 'parse', 'this', 'too', '\n', "'''", '\n', '\n', ':', ';', '"',
                      '!', '@', '#', '$', '%', '^', '&', '*', '(', ')', '_', '+', '-', '=']

    path = list(os.path.split(__file__))
    path[-1] = 'samples.txt'
    document_tokenizer = DocumentTokenizer(os.path.join(*path))
    assert len(document_tokenizer.tokens) == len(correct_tokens)
    assert document_tokenizer.tokens == correct_tokens
